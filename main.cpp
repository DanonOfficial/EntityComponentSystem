#include <iostream>
#include <SDL2/SDL.h>
#include "Includes/Game.h"

int main() {
    const int FPS = 60;
    const int frameDelay = 1000 / FPS;
    int frameTime;
    uint32_t frameStart;
    Game *game = new Game();
    game->init("Config.lua");
    while (game->isRunning()) {
        frameStart = SDL_GetTicks();

        game->handleEvents();
        game->update();
        game->render();

        frameTime = SDL_GetTicks() - frameStart;

        if (frameDelay > frameTime) {
            SDL_Delay(frameDelay - frameTime);
        }
    }

    game->clean();
    return 0;
}