//
// Created by danonofficial on 7/5/18.
//

#pragma once


#include "../EntityComponentSystem/Component.h"
#include "../Utils/Vector2D.h"

struct TransformationComponent : public ECS::Component {
    int speed = 2;
    Vector2D velocity;
    Vector2D position;
};


