//
// Created by danonofficial on 7/7/18.
//

#pragma once

#include <SDL2/SDL_rect.h>
#include "../EntityComponentSystem/Component.h"

struct CollisionComponent : public ECS::Component {
    CollisionComponent(int x, int y, int w, int h, bool isReason) : isReason(isReason) {
        box.x = x;
        box.y = y;
        box.w = w;
        box.h = h;
    };
    SDL_Rect box;
    //little solution for ball, that will collide with blocks, not universal component and system
    bool isReason;
};