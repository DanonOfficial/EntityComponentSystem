//
// Created by danonofficial on 7/5/18.
//

#pragma once

#include <SDL2/SDL_rect.h>
#include <SDL2/SDL_render.h>
#include "../EntityComponentSystem/Component.h"

struct SpriteComponent : public ECS::Component {
    SpriteComponent(const char *path, SDL_Renderer *renderer);
    SDL_Texture *texture;
    SDL_Rect srcRect;
    int _width, _height;
    SDL_Rect destRect;
};
