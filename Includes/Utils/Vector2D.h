//
// Created by danonofficial on 6/26/18.
//

#pragma once

class Vector2D {
public:
    Vector2D() = default;

    Vector2D(float x, float y);

    Vector2D &add(const Vector2D &vector);

    Vector2D &substract(const Vector2D &vector);

    Vector2D &multiply(const Vector2D &vector);

    Vector2D &divide(const Vector2D &vector);

    Vector2D &operator+(const Vector2D &vector2);

    Vector2D &operator-(const Vector2D &vector2);

    Vector2D &operator/(const Vector2D &vector2);

    Vector2D &operator*(const Vector2D &vector2);

    Vector2D &operator+=(const Vector2D &vector);

    Vector2D &operator-=(const Vector2D &vector);

    Vector2D &operator/=(const Vector2D &vector);

    Vector2D &operator*=(const Vector2D &vector);

    float getX() const;

    float getY() const;

    void setX(float x);

    void setY(float y);

    bool operator==(const Vector2D &rhs) const;

    bool operator!=(const Vector2D &rhs) const;

private:
    float x, y;

};


