//
// Created by danonofficial on 7/6/18.
//

#pragma once


#include "../Components/TransformationComponent.h"
#include "../EntityComponentSystem/TemplatedFilter.h"
#include "../EntityComponentSystem/System.h"
#include "../Components/SpriteComponent.h"

class MovementSystem : public ECS::System<ECS::Requires<TransformationComponent, SpriteComponent>> {
public:
    void update(int width, int height);
};

