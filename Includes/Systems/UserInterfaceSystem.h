//
// Created by danonofficial on 7/9/18.
//

#pragma once

#include "../Components/SpriteComponent.h"
#include "../Components/TransformationComponent.h"
#include "../Components/UserInterfaceComponent.h"
#include "../EntityComponentSystem/TemplatedFilter.h"
#include "../EntityComponentSystem/System.h"

class UserInterfaceSystem
        : public ECS::System<ECS::Requires<TransformationComponent, SpriteComponent, UserInterfaceComponent>> {
public:

    UserInterfaceSystem() = default;

    explicit UserInterfaceSystem(SDL_Renderer
                                 *renderer);

    /// Renders the system
    void render();

    /// Sets the render target
    /// \param renderTarget
    void setRenderer(SDL_Renderer *renderer);

private:
    SDL_Renderer *_renderer;
};