//
// Created by danonofficial on 7/5/18.
//

#pragma once


#include <SDL2/SDL_render.h>
#include "../EntityComponentSystem/System.h"
#include "../Components/SpriteComponent.h"
#include "../Components/TransformationComponent.h"

class SpriteRenderSystem : public ECS::System<ECS::Requires<TransformationComponent, SpriteComponent>> {
public:
    SpriteRenderSystem() = default;

    explicit SpriteRenderSystem(SDL_Renderer *renderer);

    /// Renders the system
    void render();

    /// Sets the render target
    /// \param renderTarget
    void setRenderer(SDL_Renderer *renderer);

private:
    SDL_Renderer *_renderer;
};


