//
// Created by danonofficial on 7/6/18.
//

#ifndef ENTITYCOMPONENTSYSTEM_PLAYERSYSTEM_H
#define ENTITYCOMPONENTSYSTEM_PLAYERSYSTEM_H

#include <SDL2/SDL_events.h>
#include "../EntityComponentSystem/System.h"
#include "../Components/TransformationComponent.h"
#include "../Components/PlayerComponent.h"

class PlayerSystem : public ECS::System<ECS::Requires<TransformationComponent, PlayerComponent>> {
public:
    PlayerSystem() = default;

    explicit PlayerSystem(SDL_Event *event) : _event(event) {}

    void setEvent(SDL_Event *event);

    void update();

private:
    SDL_Event *_event;
};

#endif //ENTITYCOMPONENTSYSTEM_PLAYERSYSTEM_H
