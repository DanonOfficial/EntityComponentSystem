//
// Created by danonofficial on 7/7/18.
//

#pragma once

#include "../EntityComponentSystem/TemplatedFilter.h"
#include "../EntityComponentSystem/System.h"
#include "../Components/TransformationComponent.h"
#include "../Components/CollisionComponent.h"

///Observer pattern, temporary solution
class CollisionSystem : public ECS::System<ECS::Requires<CollisionComponent, TransformationComponent>> {
public:
    struct Listener {///Thats how we make game responsible for handling event
        virtual ~Listener() = 0;

        virtual void handleCollision(ECS::Entity &first, ECS::Entity &second) = 0;
    };

    void update();

    void addListener(Listener &listener);

    void removeListener(Listener &listener);

private:
    std::vector<Listener *> _listeners;

};