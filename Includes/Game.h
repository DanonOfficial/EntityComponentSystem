//
// Created by danonofficial on 7/5/18.
//

#ifndef MEDIOCREENGINE_GAME_H
#define MEDIOCREENGINE_GAME_H

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>


#include "EntityComponentSystem/World.h"
#include "Systems/SpriteRenderSystem.h"
#include "Systems/MovementSystem.h"
#include "Systems/PlayerSystem.h"

extern "C" {
#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"
}

#include "../Third-Party/LuaBridge/Source/LuaBridge/LuaBridge.h"
#include "Systems/CollisionSystem.h"

class Game : public CollisionSystem::Listener {
public:
    Game();

    ~Game();

    void init(const char *pathToConfig);

    void handleEvents();

    void update();

    void render();

    void clean();

    bool isRunning() const;

private:
public:
    void handleCollision(ECS::Entity &first, ECS::Entity &second) override;

private:
    SDL_Window *window;
    SDL_Renderer *renderer;
    int height;
    int width;
    bool _isRunning;
    ECS::World _world;
    SDL_Event *event;
    ECS::Entity &board = _world.createEntity();
    ECS::Entity &ball = _world.createEntity();
    MovementSystem _movementSystem;
    SpriteRenderSystem _spriteRenderSystem;
    PlayerSystem _playerSystem;
    CollisionSystem _collisionSystem;
};


#endif //MEDIOCREENGINE_GAME_H
