//
// Created by danonofficial on 7/4/18.
//

#pragma once


#include <queue>
#include <unordered_map>
#include <cstddef>
#include <vector>
#include <iostream>
#include "Entity.h"
#include "System.h"


namespace ECS {
//WORLD AKA Components and System manager
    class BaseSystem;

    class World {
    public:
        World();


        template<typename TSystem>
        void addSystem(TSystem &system);

        template<typename TSystem>
        void removeSystem();

        template<typename TSystem>
        bool existSystem() const;

        void removeAllSystems();

        Entity &createEntity();

        void destroyEntity(Entity &entity);

        void destroyEntitiesVector(std::vector<Entity> &entities);

        void deactivateEntity(Entity &entity);

        bool isActivated(const Entity &entity) const;

        void refresh();

        void clear();

        std::size_t getEntityCount() const;

        const std::unordered_map<std::size_t, ECS::Entity> &getEntities() const;

        Entity getEntity(std::size_t index);

    private:
        void addSystem(BaseSystem &system, IdGenerator::typeId systemTypeId);

        void removeSystem(IdGenerator::typeId systemTypeId);

        bool doesSystemExist(IdGenerator::typeId systemTypeId) const;

        std::unordered_map<std::size_t, ECS::Entity> _entities;
        //std::vector<Entity> _entities;
        std::queue<std::size_t> _idPool;
        using SystemArray = std::unordered_map<IdGenerator::typeId, std::unique_ptr<BaseSystem>>;
        SystemArray _systems;
    };


#include "../../Src/World.ipp"

}


