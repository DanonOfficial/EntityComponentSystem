//
// Created by danonofficial on 7/4/18.
//

#pragma once


#include <vector>
#include "../../../EntityComponentSystem/Includes/EntityComponentSystem/TemplatedFilter.h"
#include "../../../EntityComponentSystem/Includes/EntityComponentSystem/Entity.h"

namespace ECS {
    class World;

    class BaseSystem {
    public:
        BaseSystem(const FilterInside::Filter &filter);

        /// Destructor
        virtual ~BaseSystem() = default;

        // return The Component Filter attached to the System
        const FilterInside::Filter &getFilter() const;

        // return The World attached to the System
        World &getWorld() const;

        // return All the entities that are within the System
        const std::vector<Entity> &getEntities() const;

    private:
        /// Initializes the system, when a world is successfully attached to it.
        virtual void initialize() {}

        /// Occurs when an Entity is added to the system
        /// \param entity The Entity that is added to the system
        virtual void onEntityAdded(Entity &entity) {}

        /// Occurs when an Entity is removed to the system
        /// \param entity The Entity that is removed from the system
        virtual void onEntityRemoved(Entity &entity) {}


        /// Used to add an Entity to the system
        /// \param entity The Entity you wish to add
        /// \note This is called by the attached World object
        void add(Entity &entity);

        /// Used to remove an Entity from the system
        /// \param entity The Entity you wish to remove
        /// \note This is called by the attached World object
        void remove(Entity &entity);

        /// Used to set the attached World
        /// \param world The World to attach to
        /// \note This is called by the attached World object
        void setWorld(World &world);


        /// The World attached to the system
        World *_world;

        /// The component filter
        FilterInside::Filter _filter;

        /// The Entities that are attached to this system
        std::vector<Entity> _entities;

        friend World;//OK that's the easiest way to deal with it, just accept it
    };


}

