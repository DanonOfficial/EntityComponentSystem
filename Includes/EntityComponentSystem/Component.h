//
// Created by danonofficial on 7/4/18.
//

#pragma once

#include <type_traits>
#include <vector>
#include "IdGenerator.h"


namespace ECS {
    class Component {
    public:
        virtual ~Component() {}
    };

    template<class T, class = typename std::enable_if<std::is_base_of<Component, T>::value>::type>
    using ComponentPtr = T *;


    template<class T>
    IdGenerator::typeId componentTypeId() {
        return IdGenerator::TypeId<Component>::GetTypeId<T>();
    }
}

