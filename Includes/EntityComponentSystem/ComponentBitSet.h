//
// Created by danonofficial on 7/5/18.
//

#ifndef MEDIOCREENGINE_COMPONENTBITSET_H
#define MEDIOCREENGINE_COMPONENTBITSET_H

#include <cstddef>
#include <bitset>

namespace ECS {
    constexpr std::size_t maxComponents = 256;
    using ComponentBitSet = std::bitset<maxComponents>;
}
#endif //MEDIOCREENGINE_COMPONENTBITSET_H
