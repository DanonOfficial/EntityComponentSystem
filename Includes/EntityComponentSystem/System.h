//
// Created by danonofficial on 7/4/18.
//

#pragma once


#include "TemplatedFilter.h"
#include "IdGenerator.h"
#include "BaseSystem.h"

namespace ECS {

    template<class RequireList, class ExcludeList = Excludes<>>
    class System : public BaseSystem {
    public:

        /// Default constructor
        System() :
                BaseSystem{FilterInside::MakeFilter<RequireList, ExcludeList>()} {
        }
    };


    template<class T>
    IdGenerator::typeId SystemTypeId() {
        return IdGenerator::TypeId<BaseSystem>::GetTypeId<T>();
    }
}


