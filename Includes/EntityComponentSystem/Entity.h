//
// Created by danonofficial on 7/4/18.
//

#pragma once

#include <cstddef>
#include <bitset>
#include <array>
#include <memory>
#include <algorithm>
#include <iostream>
#include "Component.h"
#include "ComponentBitSet.h"


namespace ECS {

    using ComponentArray = std::array<Component *, maxComponents>;

    class World;

    class Entity {
    public:
        Entity();

        Entity(World &world, size_t id);

        /// Default copy/move ctors and assignment operators
        Entity(const Entity &) = default;

        Entity(Entity &&) = default;

        Entity &operator=(const Entity &) = default;

        Entity &operator=(Entity &&) = default;

        bool isDestroyed() const;

        size_t getId() const;

        World &getWorld() const;

        bool isActivated() const;

        void activate();

        void deactivate();

        void destroy();

        template<typename T, typename... Args>
        T &addComponent(Args &&... args);

        template<typename T>
        void removeComponent();

        void removeAllComponents();

        template<typename T>
        T &getComponent() const;

        template<typename T>
        bool hasComponent() const;

        ComponentBitSet getComponents() const;

        bool operator==(const Entity &entity) const;

        bool operator!=(const Entity &entity) const { return !operator==(entity); }


    private:

        std::vector<Component *> components;
        ComponentBitSet componentBitSet;
        ComponentArray componentArray;
        bool _isActivated, _isDestroyed;
        size_t _id;
        World *_world;
    };

#include "../../Src/Entity.ipp"

}

