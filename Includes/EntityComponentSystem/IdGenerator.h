//
// Created by danonofficial on 7/4/18.
//

#pragma once

#include <cstddef>
#include <atomic>

///This code generate unique ID for every writed type(for code not object)
namespace ECS {
    namespace IdGenerator {
        typedef size_t typeId;

        template<typename Base>
        class TypeId {
        public:

            template<typename T>
            static typeId GetTypeId() {
                static const typeId id = _nextTypeId++;
                return id;
            }

        private:

            static std::atomic<typeId> _nextTypeId;
        };

        ///First init
        template<typename Base>
        std::atomic<typeId> TypeId<Base>::_nextTypeId{0};
    }
}

