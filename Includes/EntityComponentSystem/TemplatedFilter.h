//
// Created by danonofficial on 7/4/18.
//

#pragma once

#include <bitset>
#include "ComponentBitSet.h"
#include "Component.h"


#define MAX_AMOUNT_OF_COMPONENTS 1000
namespace ECS {
    //constexpr std::size_t maxComponents = 256;

    namespace FilterInside {
        template<class... Args>
        struct TypeList {
        };

        struct BaseRequires {
        };
        struct BaseExcludes {
        };

        class Filter {
        public:
            Filter(ECS::ComponentBitSet requires, ECS::ComponentBitSet excludes) :
                    _requires(requires), _excludes(excludes) {}

            bool doesPassFilter(const ComponentBitSet &typeList) const;
            ///TEMPORARY PLS REMOVE AFTER TEST
            //private:
        public:
            ComponentBitSet _requires;
            ComponentBitSet _excludes;
        };

        template<class... Args>
        static ComponentBitSet types(TypeList<Args...> typeList) { return ComponentBitSet(); }


        template<class T, class... Args>
        static ComponentBitSet types(TypeList<T, Args...> typeList) {
            static_assert(std::is_base_of<Component, T>::value, "Invalid component");
            return ComponentBitSet().set(componentTypeId<T>()) | types(TypeList<Args...>());
        }

        template<class RequireList, class ExcludeList>
        Filter MakeFilter() {
            static_assert(std::is_base_of<BaseRequires, RequireList>::value, "RequireList is not a requirement list");
            static_assert(std::is_base_of<BaseExcludes, ExcludeList>::value, "ExcludeList is not an excludes list");
            return Filter{types(RequireList{}), types(ExcludeList{})};
        }
    }

    template<class... Args>
    struct Excludes : FilterInside::TypeList<Args...>, FilterInside::BaseExcludes {
    };

    template<class... Args>
    struct Requires : FilterInside::TypeList<Args...>, FilterInside::BaseRequires {
    };



}

