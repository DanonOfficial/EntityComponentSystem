--
-- Created by IntelliJ IDEA.
-- User: danonofficial
-- Date: 7/9/18
-- Time: 9:34 AM
-- To change this template use File | Settings | File Templates.
--

Board = {
    SpriteComponent = {
        pathToSprite = "Resources/Sprites/Board.png"
    },
    CollisionComponent = {
        isReason = false,
        box = { 0, 0, 40, 10 }
    }
}

Ball = {
    SpriteComponent = {
        pathToSprite = "Resources/Sprites/Ball.png"
    },
    CollisionComponent = {
        isReason = true,
        box = { 0, 0, 32, 32 }
    }
}

Block = {
    SpriteComponent = {
        pathToSprite = "Resources/Sprites/Block.png"
    },
    CollisionComponent = {
        isReason = false,
        box = { 0, 0, 70, 32 }
    }
}