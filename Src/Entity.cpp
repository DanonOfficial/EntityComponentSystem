//
// Created by danonofficial on 7/4/18.
//
#include "../Includes/EntityComponentSystem/Entity.h"

ECS::Entity::Entity() : _world(nullptr), _isActivated(false), _isDestroyed(false) {}


ECS::Entity::Entity(World &world, size_t id) : _world(&world), _id(id), _isActivated(false), _isDestroyed(false) {}

bool ECS::Entity::isDestroyed() const {
    return _isDestroyed;
}

size_t ECS::Entity::getId() const {
    return _id;
}

ECS::World &ECS::Entity::getWorld() const {
    return *_world;
}

bool ECS::Entity::isActivated() const {
    return _isActivated;
}

void ECS::Entity::activate() {
    _isActivated = true;
}

void ECS::Entity::deactivate() {
    _isActivated = false;
}

void ECS::Entity::destroy() {
    _isDestroyed = true;
}

void ECS::Entity::removeAllComponents() {
    this->components.erase(components.begin(), components.end());
    this->componentArray.fill(nullptr);
    this->componentBitSet.reset();
}

ECS::ComponentBitSet ECS::Entity::getComponents() const {
    return this->componentBitSet;
}

bool ECS::Entity::operator==(const ECS::Entity &entity) const {
    return _id == entity._id && _world == entity._world;
}

