//
// Created by danonofficial on 6/26/18.
//

#include "../../Includes/Utils/Vector2D.h"

Vector2D::Vector2D(float x, float y) : x(x), y(y) {

}

Vector2D &Vector2D::add(const Vector2D &vector) {
    this->x += vector.x;
    this->y += vector.y;
    return *this;
}

Vector2D &Vector2D::substract(const Vector2D &vector) {
    this->x -= vector.x;
    this->y -= vector.y;
    return *this;
}

Vector2D &Vector2D::multiply(const Vector2D &vector) {
    this->x *= vector.x;
    this->y *= vector.y;
    return *this;
}

Vector2D &Vector2D::divide(const Vector2D &vector) {
    this->x /= vector.x;
    this->y /= vector.y;
    return *this;
}

Vector2D &Vector2D::operator+(const Vector2D &vector2) {
    return *(new Vector2D(this->x + vector2.x, this->y + vector2.y));
}

Vector2D &Vector2D::operator-(const Vector2D &vector2) {
    return *(new Vector2D(this->x - vector2.x, this->y - vector2.y));;
}

Vector2D &Vector2D::operator/(const Vector2D &vector2) {
    return *(new Vector2D(this->x / vector2.x, this->y / vector2.y));;
}

Vector2D &Vector2D::operator*(const Vector2D &vector2) {
    return *(new Vector2D(this->x * vector2.x, this->y * vector2.y));;
}

Vector2D &Vector2D::operator+=(const Vector2D &vector) {
    return this->add(vector);
}

Vector2D &Vector2D::operator-=(const Vector2D &vector) {
    return this->substract(vector);
}

Vector2D &Vector2D::operator/=(const Vector2D &vector) {
    return this->divide(vector);
}

Vector2D &Vector2D::operator*=(const Vector2D &vector) {
    return this->multiply(vector);
}

float Vector2D::getX() const {
    return x;
}

float Vector2D::getY() const {
    return y;
}

void Vector2D::setX(float x) {
    Vector2D::x = x;
}

void Vector2D::setY(float y) {
    Vector2D::y = y;
}

bool Vector2D::operator==(const Vector2D &rhs) const {
    return x == rhs.x &&
           y == rhs.y;
}

bool Vector2D::operator!=(const Vector2D &rhs) const {
    return !(rhs == *this);
}
