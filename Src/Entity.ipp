//
// Created by danonofficial on 7/4/18.
//

template<typename T, typename... Args>
T &Entity::addComponent(Args &&... args) {
    static_assert(std::is_base_of<Component, T>(), "T is not a component, cannot add T to entity");
    T *c(new T(std::forward<Args>(args)...));
    Component *uPtr{c};
    components.emplace_back(std::move(uPtr));

    componentArray[componentTypeId<T>()] = c;

    componentBitSet[componentTypeId<T>()] = true;
    return *c;
}

template<typename T>
void Entity::removeComponent() {
    static_assert(std::is_base_of<Component, T>(), "T is not a component, cannot remove T from entity");
    componentBitSet[componentTypeId<T>()] = false;
    componentArray[componentTypeId<T>()] = nullptr;//If i get it right, then in vector my ptr will be nullptr
    components.erase(std::remove_if(std::begin(components), std::end(components), [](Component *component) {
        return component ==
               nullptr;
    }), std::end(components));
}

template<typename T>
T &Entity::getComponent() const {
    static_assert(std::is_base_of<Component, T>(), "T is not a component, cannot retrieve T from entity");
    auto ptr(componentArray[componentTypeId<T>()]);
    return *static_cast<T *>(ptr);
}

template<typename T>
bool Entity::hasComponent() const {
    static_assert(std::is_base_of<Component, T>(), "T is not a component, cannot determine if entity has T");
    return componentBitSet[componentTypeId<T>()];
}