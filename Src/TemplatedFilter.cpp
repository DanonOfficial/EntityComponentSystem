//
// Created by danonofficial on 7/5/18.
//

#include "../Includes/EntityComponentSystem/TemplatedFilter.h"

bool ECS::FilterInside::Filter::doesPassFilter(const ComponentBitSet &typeList) const {
    for (std::size_t i = 0; i < _requires.size(); ++i) {
        if (_requires[i] == true && typeList[i] == false) {
            return false;
        }
    }

    if ((_excludes & typeList).any()) {
        return false;
    }

    return true;
}
