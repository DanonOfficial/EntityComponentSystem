//
// Created by danonofficial on 7/5/18.
//

#include "../Includes/EntityComponentSystem/World.h"

ECS::World::World() {

}


void ECS::World::removeAllSystems() {
    _systems.clear();
}

ECS::Entity &ECS::World::createEntity() {
    if (_idPool.empty()) {
        _entities.insert(std::pair(_entities.size(), Entity(*this, _entities.size())));
        //_entities.emplace_back(*this, _entities.size());
        return _entities.at(_entities.size() - 1);
    } else {///
        _entities[_idPool.front()] = Entity(*this, _idPool.front());
        //_entities.emplace(_entities.begin() + _idPool.front(), *this, _idPool.front());
        size_t returnIndex = _idPool.front();
        return _entities[returnIndex];
    }

}


void ECS::World::destroyEntity(Entity &entity) {
    ///Particially work
    _entities[entity.getId()].destroy();
    //entity.destroy();
}

void ECS::World::destroyEntitiesVector(std::vector<Entity> &entities) {
    for (auto &i: entities) {
        i.destroy();
    }
}

void ECS::World::deactivateEntity(Entity &entity) {
    entity.deactivate();
}

bool ECS::World::isActivated(const Entity &entity) const {
    return entity.isActivated();
}

//Major idea - reorginize storaging
void ECS::World::refresh() {
    std::vector<size_t> toDelete;
    for (auto &entity: _entities) {
        if (entity.second.isDestroyed()) {
            _idPool.push(entity.second.getId());
            toDelete.emplace_back(entity.first);
            //_entities.erase(_entities.begin() + entity.second.getId());
            continue;
        }
        for (auto &system: _systems) {
            auto systemIndex = system.first;
            if (system.second->getFilter().doesPassFilter(entity.second.getComponents())) {
                bool isRepeat = false;
                for (auto &i: system.second->getEntities()) { //Simple solution, need to rework
                    if (i.getId() == entity.second.getId()) {
                        isRepeat = true;
                        break;
                    }
                }
                if (isRepeat) {
                    break;
                }
                std::clog << "Entity with ID: " << entity.second.getId() << " Added to system with ID: " << system.first
                          << std::endl;
                system.second->add(entity.second);
            }
        }
    }
    for (auto &i: toDelete) {
        for (auto &system:_systems) {
            system.second->remove(_entities[i]);
        }
        _entities.erase(i);
    }
}

void ECS::World::clear() {
    _entities.clear();
    _systems.clear();
}

std::size_t ECS::World::getEntityCount() const {
    return _entities.size();
}


ECS::Entity ECS::World::getEntity(std::size_t index) {
    return _entities[index];
}

const std::unordered_map<std::size_t, ECS::Entity> &ECS::World::getEntities() const {
    return _entities;
}

void ECS::World::addSystem(ECS::BaseSystem &system, ECS::IdGenerator::typeId systemTypeId) {
    _systems[systemTypeId].reset(&system);

    system._world = this;
    system.initialize();
}

void ECS::World::removeSystem(ECS::IdGenerator::typeId systemTypeId) {
    _systems.erase(systemTypeId);
}

bool ECS::World::doesSystemExist(ECS::IdGenerator::typeId systemTypeId) const {
    return _systems.find(systemTypeId) != _systems.end();
}

