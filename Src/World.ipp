//
// Created by danonofficial on 7/5/18.
//


template<class TSystem>
void World::addSystem(TSystem &system) {
    static_assert(std::is_base_of<BaseSystem, TSystem>(), "Template argument does not inherit from BaseSystem");
    addSystem(system, SystemTypeId<TSystem>());
}

template<class TSystem>
void World::removeSystem() {
    static_assert(std::is_base_of<BaseSystem, TSystem>(), "Template argument does not inherit from BaseSystem");
    removeSystem(SystemTypeId<TSystem>());
}

template<class TSystem>
bool World::existSystem() const {
    static_assert(std::is_base_of<BaseSystem, TSystem>(), "Template argument does not inherit from BaseSystem");
    return doesSystemExist(SystemTypeId<TSystem>());
}
