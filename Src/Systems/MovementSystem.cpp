//
// Created by danonofficial on 7/6/18.
//

#include "../../Includes/Systems/MovementSystem.h"

void MovementSystem::update(int width, int height) {
    auto entities = getEntities();
    for (auto &entity: entities) {
        if (entity.getComponent<TransformationComponent>().velocity != Vector2D(0, 0)) {
            int x = entity.getComponent<TransformationComponent>().position.getX();
            int y = entity.getComponent<TransformationComponent>().position.getY();
            if (x < 0 || x > width - entity.getComponent<SpriteComponent>()._width) {
                entity.getComponent<TransformationComponent>().velocity.setX(
                        -entity.getComponent<TransformationComponent>().velocity.getX());
            }
            if (y < 0 || y > height) {
                entity.getComponent<TransformationComponent>().velocity.setY(
                        -entity.getComponent<TransformationComponent>().velocity.getY());
            }
        }
        entity.getComponent<TransformationComponent>().position += Vector2D(
                entity.getComponent<TransformationComponent>().velocity.getX() *
                entity.getComponent<TransformationComponent>().speed,
                entity.getComponent<TransformationComponent>().velocity.getY() *
                entity.getComponent<TransformationComponent>().speed);
        entity.getComponent<SpriteComponent>().destRect.x = entity.getComponent<TransformationComponent>().position.getX();
        entity.getComponent<SpriteComponent>().destRect.y = entity.getComponent<TransformationComponent>().position.getY();
    }
}

