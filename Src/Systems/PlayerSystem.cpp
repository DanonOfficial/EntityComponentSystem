//
// Created by danonofficial on 7/6/18.
//


#include "../../Includes/Systems/PlayerSystem.h"

void PlayerSystem::update() {
    if ((*_event).type == SDL_KEYDOWN) {
        auto entities = getEntities();
        for (auto &entity: entities) {
            TransformationComponent &transformComponent = entity.getComponent<TransformationComponent>();
            switch ((*_event).key.keysym.sym) {
                case SDLK_w:
                    transformComponent.velocity.setY(-1);
                    break;
                case SDLK_s:
                    transformComponent.velocity.setY(1);
                    break;
                case SDLK_d:
                    transformComponent.velocity.setX(1);
                    break;
                case SDLK_a:
                    transformComponent.velocity.setX(-1);
                    break;
                default:
                    break;
            }
        }

    }

    if ((*_event).type == SDL_KEYUP) {
        auto entities = getEntities();
        for (auto &entity: entities) {
            TransformationComponent &transformComponent = entity.getComponent<TransformationComponent>();
            switch ((*_event).key.keysym.sym) {
                case SDLK_w:
                    transformComponent.velocity.setY(0);
                    break;
                case SDLK_s:
                    transformComponent.velocity.setY(0);
                    break;
                case SDLK_d:
                    transformComponent.velocity.setX(0);
                    break;
                case SDLK_a:
                    transformComponent.velocity.setX(0);
                    break;
                default:
                    break;
            }
        }
    }

}

void PlayerSystem::setEvent(SDL_Event *event) {
    _event = event;
}
