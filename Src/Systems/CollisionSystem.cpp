//
// Created by danonofficial on 7/7/18.
//

#include "../../Includes/Systems/CollisionSystem.h"

bool isIntersect(const ECS::Entity &first, const ECS::Entity &second);

CollisionSystem::Listener::~Listener() {}

void CollisionSystem::update() {
    auto entities = getEntities();
    for (auto &i: entities) {
        if (i.getComponent<CollisionComponent>().isReason) {
            for (auto &j: entities) {
                if (i != j) {
                    if (isIntersect(i, j)) {
                        for (auto &listener: _listeners) {
                            listener->handleCollision(i, j);
                        }
                    }
                }
            }
        }
    }
}

void CollisionSystem::addListener(CollisionSystem::Listener &listener) {
    _listeners.emplace_back(&listener);
}

void CollisionSystem::removeListener(CollisionSystem::Listener &listener) {
    _listeners.erase(std::find(_listeners.begin(), _listeners.end(), &listener), _listeners.end());
}

bool isIntersect(const ECS::Entity &first, const ECS::Entity &second) {
    SDL_Rect firstRect, secondRect;
    firstRect.x = first.getComponent<TransformationComponent>().position.getX() +
                  first.getComponent<CollisionComponent>().box.x;
    firstRect.y = first.getComponent<TransformationComponent>().position.getY() +
                  first.getComponent<CollisionComponent>().box.y;
    firstRect.w = first.getComponent<CollisionComponent>().box.w;
    firstRect.h = first.getComponent<CollisionComponent>().box.h;
    secondRect.x = second.getComponent<TransformationComponent>().position.getX() +
                   second.getComponent<CollisionComponent>().box.x;
    secondRect.y = second.getComponent<TransformationComponent>().position.getY() +
                   second.getComponent<CollisionComponent>().box.y;
    secondRect.w = second.getComponent<CollisionComponent>().box.w;
    secondRect.h = second.getComponent<CollisionComponent>().box.h;
    return firstRect.x + firstRect.w >= secondRect.x &&
           secondRect.x + secondRect.w >= firstRect.x &&
           firstRect.y + firstRect.h >= secondRect.y &&
           secondRect.y + secondRect.h >= firstRect.y;
}