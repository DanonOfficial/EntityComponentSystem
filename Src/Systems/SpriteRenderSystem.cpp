//
// Created by danonofficial on 7/5/18.
//

#include "../../Includes/Systems/SpriteRenderSystem.h"


SpriteRenderSystem::SpriteRenderSystem(SDL_Renderer *renderer) : _renderer(renderer) {

}

void SpriteRenderSystem::render() {
    auto entities = getEntities();
    for (auto &entity: entities) {
        SDL_RenderCopy(_renderer, entity.getComponent<SpriteComponent>().texture,
                       &entity.getComponent<SpriteComponent>().srcRect,
                       &entity.getComponent<SpriteComponent>().destRect);
    }
}

void SpriteRenderSystem::setRenderer(SDL_Renderer *renderer) {
    _renderer = renderer;
}

