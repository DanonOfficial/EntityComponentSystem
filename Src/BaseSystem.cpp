//
// Created by danonofficial on 7/4/18.
//
#include <algorithm>
#include "../Includes/EntityComponentSystem/BaseSystem.h"

ECS::BaseSystem::BaseSystem(const ECS::FilterInside::Filter &filter) : _world(nullptr), _filter(filter) {
}

const ECS::FilterInside::Filter &ECS::BaseSystem::getFilter() const {
    return _filter;
}

ECS::World &ECS::BaseSystem::getWorld() const {
    return *_world;
}

const std::vector<ECS::Entity> &ECS::BaseSystem::getEntities() const {
    return _entities;
}

void ECS::BaseSystem::add(ECS::Entity &entity) {
    _entities.emplace_back(entity);
    onEntityAdded(entity);
}

void ECS::BaseSystem::remove(ECS::Entity &entity) {
    _entities.erase(std::remove(_entities.begin(), _entities.end(), entity), _entities.end());
    onEntityRemoved(entity);
}

void ECS::BaseSystem::setWorld(World &world) {
    _world = &world;
    initialize();
}
