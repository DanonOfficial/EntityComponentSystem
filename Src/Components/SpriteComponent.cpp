//
// Created by danonofficial on 7/6/18.
//

#include <SDL2/SDL_image.h>
#include "../../Includes/Components/SpriteComponent.h"

SpriteComponent::SpriteComponent(const char *path, SDL_Renderer *renderer) {
    SDL_Surface *loadedSurface = IMG_Load(path);
    if (loadedSurface == nullptr) {
        throw std::exception();
    }
    this->texture = SDL_CreateTextureFromSurface(renderer, loadedSurface);
    if (this->texture == nullptr) {
        throw std::exception();
    }
    _width = loadedSurface->w;
    _height = loadedSurface->h;
    srcRect.y = 0;
    srcRect.x = 0;
    srcRect.w = _width;
    srcRect.h = _height;
    destRect.y = 0;
    destRect.x = 0;
    destRect.w = _width;
    destRect.h = _height;
    SDL_FreeSurface(loadedSurface);
}
