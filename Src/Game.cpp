//
// Created by danonofficial on 7/5/18.
//

#include "../Includes/Game.h"

Game::Game() {
    event = new SDL_Event();

}

Game::~Game() {

}


void Game::init(const char *pathToConfig) {
    lua_State *L = luaL_newstate();
    luaL_loadfile(L, "Config.lua");
    luaL_openlibs(L);
    lua_pcall(L, 0, 0, 0);

    const char *title = (luabridge::getGlobal(L, "title").cast<std::string>()).c_str();
    int xPos = SDL_WINDOWPOS_CENTERED;
    int yPos = SDL_WINDOWPOS_CENTERED;
    int width = luabridge::getGlobal(L, "width").cast<int>();
    int height = luabridge::getGlobal(L, "height").cast<int>();
    bool fullscreen = luabridge::getGlobal(L, "fullscreen").cast<bool>();;
    int windowFlags = 0;
    if (fullscreen) {
        windowFlags = SDL_WINDOW_FULLSCREEN;
    }
    if (SDL_Init(SDL_INIT_EVERYTHING) == 0) {
        std::clog << "Inited successfully" << std::endl;

        window = SDL_CreateWindow(title, xPos, yPos, width, height, windowFlags);
        if (window) {
            std::clog << "Window created successfully" << std::endl;
        }
        renderer = SDL_CreateRenderer(window, -1, 0);
        if (renderer) {
            SDL_SetRenderDrawColor(renderer, 10, 255, 255, 255);
            std::clog << "Renderer created successfully" << std::endl;
        }
        _isRunning = true;
    } else {
        _isRunning = false;
    }
    this->height = height;
    this->width = width;
    _spriteRenderSystem.setRenderer(renderer);
    _playerSystem.setEvent(event);
    _world.addSystem(_spriteRenderSystem);
    _world.addSystem(_movementSystem);
    _world.addSystem(_playerSystem);
    _world.addSystem(_collisionSystem);
    _collisionSystem.addListener(*this);
    board.addComponent<TransformationComponent>();
    board.getComponent<TransformationComponent>().position = Vector2D(350, 590);
    board.getComponent<TransformationComponent>().velocity = Vector2D(0, 0);
    board.getComponent<TransformationComponent>().speed = 4;
    board.addComponent<SpriteComponent>("Resources/Sprites/Board.png", renderer);
    board.addComponent<PlayerComponent>();
    board.addComponent<CollisionComponent>(0, 0, board.getComponent<SpriteComponent>()._width,
                                           board.getComponent<SpriteComponent>()._height, false);
    board.activate();
    ball.addComponent<TransformationComponent>();
    ball.getComponent<TransformationComponent>().position = Vector2D(200, 300);
    ball.getComponent<TransformationComponent>().velocity = Vector2D(1, 1);
    ball.addComponent<SpriteComponent>("Resources/Sprites/Ball.png", renderer);
    ball.addComponent<CollisionComponent>(0, 0, ball.getComponent<SpriteComponent>()._width,
                                          ball.getComponent<SpriteComponent>()._height, true);
    ball.activate();

    std::vector<ECS::Entity *> blocks;
    for (int i = 0; i < 11; i++) {
        for (int j = 0; j < 5; j++) {
            blocks.push_back(&_world.createEntity());
            blocks[blocks.size() - 1]->addComponent<TransformationComponent>();
            blocks[blocks.size() - 1]->getComponent<TransformationComponent>().position = Vector2D(i * 70, j * 32);
            blocks[blocks.size() - 1]->getComponent<TransformationComponent>().velocity = Vector2D(0, 0);
            blocks[blocks.size() - 1]->addComponent<SpriteComponent>("Resources/Sprites/Block.png", renderer);
            blocks[blocks.size() - 1]->addComponent<CollisionComponent>(0, 0,
                                                                        blocks[i]->getComponent<SpriteComponent>()._width,
                                                                        blocks[i]->getComponent<SpriteComponent>()._height,
                                                                        false);
            blocks[blocks.size() - 1]->activate();
        }
    }

}

void Game::handleEvents() {

    SDL_PollEvent(event);

    switch ((*event).type) {
        case SDL_QUIT:
            _isRunning = false;
        case SDL_KEYDOWN:
        default:
            break;
    }
}

void Game::render() {
    SDL_RenderClear(renderer);
    _spriteRenderSystem.render();
    SDL_RenderPresent(renderer);
}

void Game::clean() {
    SDL_DestroyWindow(window);
    SDL_DestroyRenderer(renderer);
    SDL_Quit();
    //_world.clear();
    std::clog << "Cleaned" << std::endl;
}

bool Game::isRunning() const {
    return _isRunning;
}

void Game::update() {
    _world.refresh();
    _movementSystem.update(width, height);
    _playerSystem.update();
    _collisionSystem.update();
}

void Game::handleCollision(ECS::Entity &first, ECS::Entity &second) {


    if (first.getComponent<CollisionComponent>().isReason) {
        if (second == board) {
            first.getComponent<TransformationComponent>().velocity.setY(
                    -first.getComponent<TransformationComponent>().velocity.getY());
        } else {
            std::cout << "Entity with ID: " << first.getId() << " hitted object with ID: " << second.getId()
                      << std::endl;
            _world.destroyEntity(second);
            //TODO: rework collision logic, thats work strange
            if (second.getComponent<TransformationComponent>().position.getX() +
                second.getComponent<CollisionComponent>().box.w >
                first.getComponent<TransformationComponent>().position.getX() ||
                second.getComponent<TransformationComponent>().position.getX() <
                first.getComponent<TransformationComponent>().position.getX() +
                first.getComponent<CollisionComponent>().box.w) {
                first.getComponent<TransformationComponent>().velocity.setY(
                        -first.getComponent<TransformationComponent>().velocity.getY());
            } else {
                first.getComponent<TransformationComponent>().velocity.setX(
                        -first.getComponent<TransformationComponent>().velocity.getX());
            }
        }
    }
    /*if (second.getComponent<CollisionComponent>().isReason) {
        std::cout << "Entity with ID: " << second.getId() << " hitted object with ID: " << first.getId() << std::endl;
    }*/
}
